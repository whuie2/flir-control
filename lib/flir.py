from __future__ import annotations
import os
try:
    import PySpin
except:
    print("WARNING: error loading module 'PySpin'")
import sys
import time

def is_readable(*things):
    return all(
        PySpin.IsAvailable(thing) and PySpin.IsReadable(thing)
        for thing in things
    )

def is_writable(*things):
    return all(
        PySpin.IsAvailable(thing) and PySpin.IsWritable(thing)
        for thing in things
    )

def get_enum_node(nodemap, nodename):
    return PySpin.CEnumerationPtr(nodemap.GetNode(nodename))

def set_enum_node(nodemap, nodename, value, expect=None):
    node = get_enum_node(nodemap, nodename)
    node_value = node.GetEntryByName(value)
    if not is_writable(node) or not is_readable(node_value):
        raise Exception(expect) if expect is not None else Exception
    node.SetIntValue(node_value.GetValue())

def get_float_node(nodemap, nodename):
    return PySpin.CFloatPtr(nodemap.GetNode(nodename))

def set_float_node(nodemap, nodename, value, expect=None):
    node = get_float_node(nodemap, nodename)
    if not is_writable(node) or not is_readable(node):
        raise Exception(expect) if expect is not None else Exception
    node.SetValue(value)

def get_int_node(nodemap, nodename):
    return PySpin.CIntegerPtr(nodemap.GetNode(nodename))

def set_int_node(nodemap, nodename, value, expect=None):
    node = get_int_node(nodemap, nodename)
    if not is_writable(node) or not is_readable(node):
        raise Exception(expect) if expect is not None else Exception
    node.SetValue(value)

def configure_camera(cam, nodemap, exposure_time: float=None, gain: float=None,
        gamma: float=1.25, black_level: float=5.0, bin_size: int=1, **kwargs):
    set_enum_node(nodemap,
        "PixelFormat", "Mono16", "Couldn't set pixel format")

    #set_enum_node(nodemap,
    #    "ExposureCompensationAuto",
    #    "Off" if exposure_compensation is not None else "Continuous",
    #    "Couldn't set auto exposure compensation"
    #)
    #if exposure_compensation is not None:
    #    set_float_node(nodemap,
    #        "ExposureCompensation",
    #        exposure_compensation,
    #        "Couldn't set exposure compensation"
    #    )

    set_enum_node(nodemap,
        "ExposureAuto",
        "Off" if exposure_time is not None else "Continuous",
        "Couldn't set auto exposure time"
    )
    if exposure_time is not None:
        set_float_node(nodemap,
            "ExposureTime",
            exposure_time,
            "Couldn't set exposure time"
        )

    set_enum_node(nodemap,
        "GainAuto",
        "Off" if gain is not None else "Continuous",
        "Couldn't set auto gain"
    )
    if gain is not None:
        set_float_node(nodemap,
            "Gain", gain, "Couldn't set gain")

    set_float_node(nodemap,
        "Gamma", gamma, "Couldn't set gamma")

    set_float_node(nodemap,
        "BlackLevel", black_level, "Couldn't set black level")

    set_int_node(nodemap,
        "BinningVertical", bin_size, "Couldn't set bin size")

    # setting "BinningVertical" also sets "BinningHorizontal" to the same value
    #set_int_node(nodemap,
    #    "BinningHorizontal", bin_size, "Couldn't set bin size")

def set_trigger_mode(cam, nodemap, onoff: bool=False):
    set_enum_node(nodemap,
        "TriggerMode", "On" if onoff else "Off", "Couldn't set trigger mode")

def configure_trigger(cam, nodemap, selector: str="FrameStart",
        source: str="Line3", activation: str="RisingEdge", **kwargs):
    # disable trigger mode to set options
    set_trigger_mode(cam, nodemap, False)

    # not sure exactly what FrameStart is, but the triggering example says it's
    # the right choice
    set_enum_node(nodemap,
        "TriggerSelector", selector, "Couldn't set trigger selector")

    # trigger via hardware -- TTL on pin 4 (green) with ground on 5 (brown)
    set_enum_node(nodemap,
        "TriggerSource", source, "Couldn't set trigger source")

    set_enum_node(nodemap,
        "TriggerActivation", activation, "Couldn't set trigger activation")

    ## re-enable trigger mode
    #set_trigger_mode(cam, nodemap, True)

    return True

def acquire_frames(cam, nodemap, num_frames: int, roi: [int, 4]=None,
        timeout: int=None, printflag: bool=True):
    set_enum_node(nodemap,
        "AcquisitionMode", "Continuous", "Couldn't set acquisition mode")
    if printflag: print(f"[flir] Begin acquiring frames ({num_frames})")
    # acquire images
    frames = list()
    cam.BeginAcquisition()
    try:
        for k in range(num_frames):
            if timeout is None:
                img = cam.GetNextImage()
            else:
                img = cam.GetNextImage(int(1000 * timeout))
            if img.IsIncomplete():
                raise Exception("Incomplete image capture")
            else:
                data = img.GetNDArray()
                if roi is not None:
                    data = data[roi[1]:roi[1] + roi[3], roi[0]:roi[0] + roi[2]]
                frames.append(data)
                img.Release()
            if printflag: print(f"[flir] grab frame {k + 1} / {num_frames}")
    except BaseException:
        try:
            img.Release()
        except:
            pass
    cam.EndAcquisition()
    return frames

def connect_camera(serial: str):
    system = PySpin.System.GetInstance()
    cam_list = system.GetCameras()
    num_cameras = cam_list.GetSize()
    if num_cameras == 0:
        cam_list.Clear()
        system.ReleaseInstance()
        print("Couldn't find any cameras")
    cam = cam_list.GetBySerial(serial)
    cam.Init()
    nodemap = cam.GetNodeMap()
    return cam, nodemap, cam_list, system

def disconnect_camera(cam, cam_list, system):
    if cam.IsStreaming():
        cam.EndAcquisition()
    cam.DeInit()
    del cam
    cam_list.Clear()
    system.ReleaseInstance()

class Grasshopper:
    """
    Class to hold data and drive hardware-triggered image acquisition through
    the PySpin backend. Settings may be applicable to other Flir models, but
    perhaps not (things are far from fully tested).
    """
    def __init__(self, serial: str):
        self.serial = serial
        self.cam = None
        self.nodemap = None
        self.cam_list = None
        self.system = None

    def __getstate__(self):
        return self.__dict__.copy()

    def __setstate__(self, state):
        self.__dict__.update(state)

    def connect(self):
        assert self.cam is None, "Already connected to the camera"
        self.cam, self.nodemap, self.cam_list, self.system \
                = connect_camera(self.serial)
        return self

    def _is_connected(self):
        assert self.cam is not None, "Not connected to the camera"

    def acquire_frames(self, num_frames: int, roi: list[int, 4]=None,
            timeout: float=None, printflag: bool=True):
        self._is_connected()
        set_trigger_mode(self.cam, self.nodemap, True)
        return acquire_frames(
            self.cam, self.nodemap, num_frames, roi, timeout, printflag)

    def configure_trigger(self, selector: str="FrameStart", source: str="Line3",
            activation: str="RisingEdge", **kwargs):
        self._is_connected()
        configure_trigger(self.cam, self.nodemap, selector, source, activation,
            **kwargs)
        return self

    def configure_capture(self, exposure_time: float=None, gain: float=None,
            gamma: float=1.25, black_level: float=5.0, bin_size: int=1,
            **kwargs):
        self._is_connected()
        configure_camera(
            self.cam, self.nodemap,
            exposure_time, gain, gamma, black_level, bin_size)
        return self

    def disconnect(self):
        if self.cam.IsStreaming():
            self.cam.EndAcquisition()
        self.cam.DeInit()
        del self.cam
        self.cam = None
        del self.nodemap
        self.nodemap = None
        self.cam_list.Clear()
        del self.cam_list
        self.cam_list = None
        self.system.ReleaseInstance()
        del self.system
        self.system = None
        return self

